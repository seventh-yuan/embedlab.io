# cocotb的使用

cocotb是一个开源的基于协程的联合仿真测试工具，使得开发者可以使用python来仿真验证VHDL和SystemVerilog RTL.

## 环境搭建

cocotb需要以下开发环境

- Python3.6+
- GNU Make 3+
- Verilog 或 VHDL模拟器

安装`cocotb`和`iverilog`

```
pip install cocotb
```
安装`iverilog`

```
sudo apt-get install iverilog
```