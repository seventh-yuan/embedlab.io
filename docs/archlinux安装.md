# archlinux 虚拟机安装

**使用archinstall命令安装archlinux**

**添加用户**

```
useradd -m -G wheel -s /bin/bash archlinux
passwd archlinux
```

编译`/etc/sudoers`, 取消wheel用户组前的注释符号

![alt text](image.png)

**安装基础组件**


```
sudo pacman -S net-tools vim git less bear clang bash-completion
```

**安装gnome**

```
sudo pacman -S gnome
sudo systemctl enable gdm
```

**安装中文字体和输入法**

1. 编辑`/etc/locale.gen` 取消`en_US.UTF-8 UTF-8`, `zh_CN.GB18030 GB18030`, `zh_CN.GBK GBK`, `zh_CN.UTF-8 UTF-8`, `zh_CN GB2312`的注释， 然后执行如下命令生成配置

```
sudo locale-gen
```

2. 安装中文字体和输入法

```
sudo pacman -S ttf-roboto noto-fonts noto-fonts-cjk adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts ttf-dejavu

sudo pacman -S ibus-libpinyin
```

**安装yay**

1. 配置go代理

```
echo "export GO111MODULE=on" >> ~/.bashrc
echo "export GOPROXY=https://goproxy.cn" >> ~/.bashrc
source ~/.bashrc
```

2. 安装yay
```
git clone https://aur.archlinux.org/yay.git
makepkg -si
```

**安装docker**

```
yay -S docker
udo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker $USER
```
重启系统

**安装samba**

```
yay -S samba
```

编译`/etc/samba/smb.conf`, 添加如下内容

```
[archlinux]
	comment = archlinux workspace
	path = /home/yuanle/workspace
	valid users = yuanle
	guest ok = yes
	writable = yes
	browsable = yes

```

添加用户

```
sudo smbpasswd -a yuanle
```
启动smb服务
```
sudo systemctl enable smb
sudo systemctl start smb
```

**安装ssh服务**

```
yay -S openssh
systemctl enable sshd
systemctl start sshd
```

**终端颜色配置**

```
yay -S gentoo-bashrc
cp /usr/share/gentoo-bashrc/bashrc ~/.bashrc
source ~/.bashrc
```

